from bs4 import BeautifulSoup
import requests,csv
import numpy as np
import pandas as pd
import os
import re
import wget

# Utility Function

def get_podcasts(site,l1,l2,trans):
    i=1
    flag_name=""
    while(True):
        response=requests.get("https://changelog.com/"+site+"?page="+str(i)+"#feed").text
        soup=BeautifulSoup(response,'lxml')
        div=soup.find("div",class_="feed")
        articles=div.findAll('article')
        
        cnt=0
        for j in articles:
            name=j.header.h2.a.text
            print(name)
            audio_link=j.header.h2.a.get("href")
            if(cnt==0):
                if(flag_name==name):
                    break
                flag_name=name
                cnt+=1

            l=audio_link.split("/")
            
            if(l[len(l)-1] in trans[0]):
                l1.append(name)
                response=requests.get(audio_link).text
                soup=BeautifulSoup(response,'lxml')
                a=soup.find("a",class_="toolbar_item toolbar_item--button playbar playbar_play")
                audio_link=a.get("href")
                l2.append(audio_link)
                l3.append(''+site+'/'+trans[1]+''+l[len(l)-1]+'.md')
    
        if(flag_name==name):
            break
        i+=1
        
    return l1,l2

def get_trans():
    trans={}
    cnt=0
    temp=[]
    for a,b,c in os.walk('transcripts'):
        if '.git' in b:
            b.remove('.git')

        if cnt==0:
            podcasts=b
        
        for j in b:
        
            var=["away-from-keyboard-","go-time-","js-party-","request-for-commits-",
                 "practical-ai-","spotlight-","founders-talk-","the-changelog-",
                 "brain-science-","backstage-"]
            
            for x,y,z in os.walk('transcripts/'+j):
                
                for i in range(len(var)):
                    if var[i] in z[0]:
                        break
                
                for k in z:
                    k=k.replace(var[i],"")
                    temp.append(k.split('.')[0])
                
                
                trans[j]=[temp,var[i]]
                temp=[]

            cnt+=1
    return trans,podcasts,var

def generate_csv(l1,l2,l3):
    file=open('scrape.csv','w')
    csv_writer=csv.writer(file)
    csv_writer.writerow(['name','audio_link','trans_link'])
    
    def remove_non_ascii(text):
        return ''.join(i for i in text if ord(i)<128)
    
    for i in range(len(l1)):
        l1[i]=remove_non_ascii(l1[i])
        csv_writer.writerow([l1[i],l2[i],l3[i]])
        
    file.close()

# Data Preparation

l1=[]
l2=[]
l3=[]

trans,podcasts,var=get_trans()

for i in range(len(podcasts)):
    l1,l2=get_podcasts(podcasts[i],l1,l2,trans[podcasts[i]])

generate_csv(l1,l2,l3)

# Cleaning Transcript

data=pd.read_csv('scrape.csv')

os.system('mkdir -p data')

for i in range(len(data)):
    f=open('transcripts/'+data['trans_link'].iloc[i],'r')
    content=f.read()
    dir=data['trans_link'].iloc[i].split("/").pop().split(".")[0]
    os.system('cd data && mkdir '+dir+' && cd ..')
    f=open('data/'+dir+'/'+dir+'.txt','w')
    wget.download(data['audio_link'][i],'data/'+dir)
    os.system('ffmpeg -i data/'+dir+'/'+dir+'.mp3 data/'+dir+'/'+dir+'.wav && rm -f data/'+dir+'/'+dir+'.mp3')

    pattern=re.compile(r'(\*\*[a-zA-Z ]+:\*\*|\\\[[a-zA-Z0-9:.]+\\\]|\(.+\))|\.\.\.|\-\-|\;')
    ans=re.subn(pattern,'',content)
    f.write(ans[0])
    data['trans_link'].iloc[i]=data['trans_link'].iloc[i].split(".")[0]+'.txt'
    